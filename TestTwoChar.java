package edu.mercer.medapps;

/** imports */

/** Random number */

import java.util.Random;

/** JUnit assertion tests */


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * <b>TestTwoChar</b>: JUnit test class 
 * 
 * <ul><li>PasswordEntropy
 *     <li>input: two Character password
 *     <li>output: entropy calculation
 * </ul>
 * 
 * @see Config
 * 
 * @author Hatfield, Kevin
 */

public class TestTwoChar implements Config
{	
	private static Integer _numTests = 0;
	
	/** common error message for this class, JUnit tests when run in suite */
	
	private static final String _errMsg = "Two Characters";

	/** initialization data for testing assert Equal */

	private static final Character[] _equalCharArr = {'0','9'};

	/** data for testing assert Equal */

	private static final PasswordStringBuilder _equalData  
	
		= new PasswordStringBuilder( _equalCharArr );

	/** initialization data for testing assert UnEqual */

	private static final Character[] _inequalityDataArr = {'a','Z'};
	
	/** data for testing assert UnEqual */
	
	private static final PasswordStringBuilder _inequalityData  
	
		= new PasswordStringBuilder(_inequalityDataArr);
		
	/** test object containing shared code for JUnit tests */
	
	private static final PasswordEntropy _testFramework 
	
		= new PasswordEntropy( _equalData );
	
	
	/** encapsulation methods */

	// most unneeded: instance variables final

	
	/** @return number of this type of test to run */
	
	private Integer get_numTests()
	{
		return _numTests > 0 ? _numTests : control.numTests.val();
				
	}//method

	
	/** @param numTestsIn Integer number of this type of test to run */
	
	protected void set_numTests(Integer numTestsIn)
	{
		_numTests = numTestsIn;
		
	}//method

	
	/** functional methods */
	
	
	/** verify successful object creation for test class */

	@Before
	public void setUp()
	{	
		//valid outside try block: constructors should not throw exceptions
		
		assertEquals( _errMsg, _testFramework, 
				
				new PasswordEntropy(_equalData) );

		assertNotEquals( _errMsg, _testFramework, 
				
				new PasswordEntropy(_inequalityData) );
		
	}//method

	
	@Test
	public void test()
	{
		try
		{			
			for( Integer count=0; count < get_numTests(); count++ )
			{ 
				twoCharacters();
				
			}//for
		}
		catch ( Exception twoCharTestException )
		{
			fail( "Abend: " + twoCharTestException.getMessage() );
			
		}//try
		
	}//method
		 
	 /**
	  * testTwoCharacters
	  *  
	  * <ol>
	  * <li>create Character array of random ASCII subset letters, symbols, 
	  * space, numbers
	  * <li>test entropy of distinct
	  * <li>test entropy of repeated
	  * </ol>
	  * 
	  * examples:<hr>
	  * 
	  *  <table><tr><th>'Password'</th><th> : Entropy</th></tr>
	  *  
	  *  <tr><td> '11' </td><td>:  4 </td></tr>
	  *  <tr><td> '  ' </td><td>:  6 </td></tr>
	  *  <tr><td> '19' </td><td>:  6 </td></tr>
	  *  <tr><td> 'kk' </td><td>:  7 </td></tr>
	  *  <tr><td> ' #' </td><td>:  8 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> '% ' </td><td>:  8 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> ' 1' </td><td>:  9 </td></tr>
	  *  <tr><td> '%1' </td><td>:  9 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> 'QZ' </td><td>:  9 </td></tr>
	  *  <tr><td> 'U#' </td><td>: 10 </td></tr>
	  *  <tr><td> 'u#' </td><td>: 10 </td></tr>
	  *  <tr><td> '%J' </td><td>: 10 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> ' J' </td><td>: 10 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> 'Z ' </td><td>: 10 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> 'Z1' </td><td>: 10 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> 'z1' </td><td>: 10 </td></tr>                                                                                                                                                                                  
	  *  <tr><td> 'kK' </td><td>: 11 </td></tr>
	  *  <tr><td> 'Qz' </td><td>: 11 </td></tr></table>
	  *  
	  *  <i>quotes shown for clarity of whitespace</i>
	  * @throws Exception 
	  */

	 public void twoCharacters() throws Exception
	 {
		 /** get two random characters */

		 Character[] rdmChr = UtilForTests.randomChars( 2, false, false );

		 if( control.verbose.isSet() ) 
		 {
			 System.out.print( "Random chars: '" + 
		 
					 rdmChr[0] + rdmChr[1] + "' " );
		 
		 }//if
		 
		 /** test repeated character password */

		 Character[] repeatChr = new Character[2];

		 repeatChr[0] = rdmChr[0];
		 repeatChr[1] = rdmChr[0];

		 final PasswordStringBuilder repeatChrPw = 

				 new PasswordStringBuilder( repeatChr );

		 final PasswordEntropy repeatPwdEntropy = 

				 new PasswordEntropy( repeatChrPw );

		 final Integer repeatEntropy = repeatPwdEntropy.value();

		 if( ! repeatChrPw.repeats() )
		 {
			 fail( "Password has non-repeating characters" );

		 }//if

		 if( repeatChrPw.isNumeric() )
		 {
			 assertEquals( _errMsg, (Integer) 4, repeatEntropy );
			 
		 }
		 else if( repeatChrPw.allSpaces() || repeatChrPw.allSymbols() ) 
		 {	 
			 // '  ' or '%%' = 6

			 assertEquals( _errMsg, (Integer) 6, repeatEntropy );	

		 }
		 else if( repeatChrPw.ucase() || repeatChrPw.lcase() ) 
		 {	 
			 // 'KK' or 'gg' = 7

			 assertEquals( _errMsg, (Integer) 7, repeatEntropy );	

		 }//if

		 /** test possibly-distinct characters password */

		 final PasswordStringBuilder distinctPwd =

				 new PasswordStringBuilder( rdmChr );

		 final PasswordEntropy distinctPwdEntropy = 

				 new PasswordEntropy( distinctPwd );

		 final Integer distinctEntropy = distinctPwdEntropy.value();

		 if( control.verbose.isSet() ) 
		 {
			 System.out.println( "; Entropy '" + rdmChr[0] + rdmChr[1] + 
					 
					 "' = " + distinctEntropy );		 
		 }//if
		 
		 assertFalse( "Possibly distinct characters entropy exceeds " + 
		 
				 "repeating char password", repeatEntropy > distinctEntropy );
		 
		 if( distinctPwd.repeats() )
		 {
			 return;	// test already done above
			
		 }//if
		 
		 if( distinctPwd.hasSpaces() ) 
		 {	 
			 if( distinctPwd.hasSymbols() )
			 {
				 // ' $' or '* ' = 8

				 assertEquals( _errMsg, (Integer) 8, distinctEntropy );

			 }
			 else if( distinctPwd.hasNumeric() )
			 {
				 // ' 4' = 9

				 assertEquals( _errMsg, (Integer) 9, distinctEntropy );
				 
			 }
			 else
			 {
				 // ' K' or 'g ' = 10

				 assertEquals( _errMsg, (Integer) 10, distinctEntropy );

			 }//if
		 }
		 else
		 {
			 // two distinct non-blank, characters

			 if( distinctPwd.hasSymbols() )
			 {
				 if( distinctPwd.hasNumeric() )
				 {
					 // '%4' = 9

					 assertEquals( _errMsg, (Integer) 9, distinctEntropy );
				 
				 }
				 else
				 {
					 // '%K' or 'g%' = 10

					 assertEquals( _errMsg, (Integer) 10, distinctEntropy );

				 }//if
			 }
			 else
			 {
				 //no spaces, no symbols
				 
				 if( distinctPwd.isNumeric() )
				 {
					 // '01' = 6
					 
					 assertEquals( _errMsg, (Integer) 6, distinctEntropy );
					 
				 }
				 else if( distinctPwd.hasNumeric() )
				 {
					 // 'd8' or 'D4' = 10

					 assertEquals( _errMsg, (Integer) 10, distinctEntropy );
					 
				 }
				 else
				 {
					 if( distinctPwd.ucase() || distinctPwd.lcase() )
					 {
						 // 'de' or 'LI' = 9
						 
						 assertEquals( _errMsg, (Integer) 9, distinctEntropy );
						 
					 }
					 else
					 {
						 // 'dE' or 'Li' = 11
						 
						 assertEquals( _errMsg, (Integer) 11, distinctEntropy );
						 
					 }//if
					 
				 }//if
				 
			 }//if

		 }//if

	 }//method

	 
}//class
