package edu.mercer.medapps;

import java.util.Random;

/**
 * static Methods shared by JUnit tests
 * 
 * @see Config
 * 
 * @author Hatfield, Kevin
 */

public class UtilForTests implements Config
{
	/** 
	 * <b>randomChars</b><br><br>
	 * 
	 * create Character array: random Characters of limited ASCII
	 * 
	 * @param sizeIn Integer return Character[] of this length
	 * 
	 * @param numericIn Boolean Character[] should contain only numbers
	 * 
	 * @param uniqIn Boolean Character[] should only have distinct elements
	 * 
	 * @return Character[] containing random limited ASCII
	 * 
	 * @see Config
	 */

	static protected Character[] randomChars(Integer sizeIn, Boolean numericIn, 
			 
			 Boolean uniqIn) throws Exception
	{
		 Character[] rdmChrArr = new Character[sizeIn];

		 for( Integer b=0; b < sizeIn; b++ ) rdmChrArr[b] = Character.MAX_VALUE;
		 
		 final Integer maxCodePoint = numericIn ? 
				 
				 ascii.NUMERIC_HIGH.val() : ascii.ALL_CHAR_HIGH.val();
		 
		 final Integer minCodePoint = numericIn ? 
				 
				 ascii.NUMERIC_LOW.val() : ascii.ALL_CHAR_LOW.val();
				 
		 for( Integer z=0; z < sizeIn; )
		 {
			 Integer rndmCodePoint = new Random().nextInt( maxCodePoint+1 );
			 
			 Integer safe = 0;
			 
			 while( rndmCodePoint < minCodePoint || 
					 
					 rndmCodePoint > maxCodePoint )
			 {
				 rndmCodePoint = new Random().nextInt( maxCodePoint+1 );

				 if( safe++ > 1000 )
				 { 
					 throw new Exception( 
						 "Looping creating random chararacter array" );
				 }//if
				 
			 }//while

			 char[] characterConv = Character.toChars(rndmCodePoint);
			 
			 //discard any leading supplementary char
			 
			 Character candidateChar = characterConv[characterConv.length-1];

			 Boolean found = false;

			 if( uniqIn )	//ensure Character is not already in Character[]
			 {
				 for( Integer x=0; x < rdmChrArr.length; x++ ) 
				 {
					 if( Character.compare(

							 rdmChrArr[x], candidateChar) == 0 )
					 {
						 found = true;
						 break;

					 }//if

				 }//for

			 }//if

			 if( !found )
			 {
				 rdmChrArr[z++] = candidateChar;

			 }//if
				 			 			 
		 }//for

		 //validate before return
		 
		 for( Integer b=0; b < sizeIn; b++ )
		 { 
			 if( Character.compare( rdmChrArr[b] , Character.MAX_VALUE ) == 0 )
			 {	 
				 throw new Exception( "Fail create random Character[], " +
				 
						 "initialization value detetcted");
				 
			 }//if
					 
			 if( (int) rdmChrArr[b] < minCodePoint )
			 {
				 throw new Exception( "Fail create random Character[], " +

						"below ascii range: " + 
						
						(int) rdmChrArr[b]);
				 
			 }//if

			 if( (int) rdmChrArr[b] > maxCodePoint )
			 {
				 throw new Exception( "Fail create random Character[], " +

						"beyond ascii range: " + 
						
						(int) rdmChrArr[b]);
				 
			 }//if
			 
		 }//for

		 return rdmChrArr;
		 
	}//method
	
	
}//class


