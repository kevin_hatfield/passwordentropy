package edu.mercer.medapps;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Test: empty password
 * 
 * @author Hatfield, Kevin
 */

public class TestNull
{	
	/** null as Character object */
	
	protected static final Character NULL_CHAR = new Character('\u0000');
	
	/** safe character representing nothing */
	
	protected static final Character ISO_EMPTY = new Character('\uFFFD'); 
	
	/** common error message for this class, JUnit tests when run in suite */
	
	private static final String _errMsg = "Null password";

	/** data for testing assert Equal */

	private static final PasswordStringBuilder _equalData = 
			
			new PasswordStringBuilder(NULL_CHAR);

	/** data for testing assert UnEqual */
	
	private static final PasswordStringBuilder _inequalityData = 
	
			new PasswordStringBuilder(ISO_EMPTY);
		
	/** test object containing shared code for JUnit tests */
	
	private static final PasswordEntropy _testFramework = 
			
			new PasswordEntropy( _equalData );
	
	/** encapsulation methods */

	// unneeded: instance variables final
	
	/** functional methods */
	
	/** verify successful object creation for test class */

	@Before
	public void setUp()
	{	
		//valid outside try block: constructors should not throw exceptions
		
		assertEquals( _errMsg, _testFramework, 
				
				new PasswordEntropy(_equalData) );

		assertNotEquals( _errMsg, _testFramework,
				
				new PasswordEntropy(_inequalityData) );
		
	}//method
	
	/** test against known- expected, desired -input and output */

	@Test
	public void test()
	{
		try
		{
			_testFramework.value(); 
					
			fail(_errMsg);
			
		}
		catch(Exception anyException)
		{
			; //nop
			
		}//try
		
	}//method
	
	
}//class
