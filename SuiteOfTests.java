package edu.mercer.medapps;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

//JUnit Suite Test

@RunWith(Suite.class)
@Suite.SuiteClasses
(
		{ 
			TestEmpty.class,
			TestNull.class,
			TestSoloChar.class,
			TestTwoChar.class
		}
)

public class SuiteOfTests{}//class