package edu.mercer.medapps;

/** 
 * <b>Config</b>: PasswordEntropy class, JUnit tests shared constants 
 * 
 * @author Hatfield, Kevin
 */

 public interface Config 
{
	/** constants used for password validation */
	
	static enum ascii
	{	
		ALL_CHAR_LOW (32),
		ALL_CHAR_HIGH (126),
		
		NUMERIC_LOW (48),
		NUMERIC_HIGH (57);
		
		private Integer _intVal;
		
		
		private ascii(Integer valueIn)
		{ 
			_intVal = valueIn; 
			
		}//constructor

		
		protected Integer val()
		{ 	
			return _intVal; 

		}//method
		
	}//enum
	
	
	/** constants, settings for running of tests */
	
	
	static enum control
	{
		numTests (1000),
		verbose (true);
		
		private Integer _intVal;
		
		private Boolean _verboseVal;

		
		private control(Integer numIn)
		{ 
			_intVal = numIn;
			
		}//constructor
		
		
		private control(Boolean verboseValIn)
		{ 
			_verboseVal = verboseValIn;
			
		}//constructor
		
		
		/** 
		 * default number of tests to run<br>
		 * 
		 * @see TestTwoChar 
		 */
		
		protected Integer val()
		{ 
			return _intVal;
			
		}//method

		
		/**
		 * display test output to stdout or default logger<br>  
		 * 
		 * example: "Random chars: 'E4' ; Entropy 'E4' = 10"
		 *  
		 * @see TestTwoChar 
		 */
		
		protected Boolean isSet()
		{ 
			return _verboseVal;
			
		}//method
		
	}//enum
	
}//interface
