package edu.mercer.medapps;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Test: single character password
 * 
 * <ul><li>limited range of ascii: letter digit symbol space</ul>
 * 
 * @see Config
 * 
 * @author Hatfield, Kevin
 */

public class TestSoloChar implements Config
{	
	/** common error message for this class, JUnit tests when run in suite */
	
	private static final String _errMsg = "Individual Character";

	/** data for testing assert Equal */

	private static final PasswordStringBuilder _equalData  
	
		= new PasswordStringBuilder(0);

	/** data for testing assert UnEqual */
	
	private static final PasswordStringBuilder _inequalityData  
	
		= new PasswordStringBuilder('x');
		
	/** test object containing shared code for JUnit tests */
	
	private static final PasswordEntropy _testFramework 
	
		= new PasswordEntropy( _equalData );
	
	/** encapsulation methods */

	// unneeded: instance variables final
	
	/** functional methods */
	
	/** verify successful object creation for test class */

	@Before
	public void setUp()
	{	
		//valid outside try block: constructors should not throw exceptions
		
		assertEquals( _errMsg, _testFramework, 
				
				new PasswordEntropy(_equalData) );

		assertNotEquals( _errMsg, _testFramework, 
				
				new PasswordEntropy(_inequalityData) );
		
	}//method
	
	
	/** test against known- expected, desired -input and output */

	
	/** example: 0 1 2 3 4 5 */
	
	@Test
	public void testDigits()
	{
		try
		{
			for(Integer digit=0; digit<10; digit++)
			{				
				PasswordStringBuilder passwordStrBld = 
						
						new PasswordStringBuilder( digit );
				
				PasswordEntropy pwTest = new PasswordEntropy( passwordStrBld );
				
				Integer entropy = pwTest.value(); 
				
				assertEquals( _errMsg, entropy, (Integer) 3 );
				
			}//for
			
		}
		catch(Exception anyException)
		{
			fail(_errMsg + " caused abend " + anyException.getMessage());
			
		}//try
		
	}//method
	
	
	/** example: a b c d */
	
	@Test
	public void testCharacters()
	{
		try
		{
			for( Integer codePoint = ascii.ALL_CHAR_LOW.val(); 
					     
				 	     codePoint < ascii.ALL_CHAR_HIGH.val() + 1; 
					
						 codePoint++)
			{								
				if( Character.isAlphabetic(codePoint) )
				{
					//convert char[] into Character[]

					char[] letterCharArr = Character.toChars(codePoint);

					Character letter = new Character(letterCharArr[0]);
					PasswordStringBuilder passwordStrBld = 

							new PasswordStringBuilder( letter );

					PasswordEntropy pwTest = new PasswordEntropy( passwordStrBld );

					Integer entropy = pwTest.value(); 

					assertEquals( _errMsg, entropy, (Integer) 4 );
				
				}//if
				
			}//for
			
		}
		catch(Exception anyException)
		{
			fail(_errMsg + " caused abend " + anyException.getMessage());
			
		}//try
		
	}//method

	
	/** example: space ? / ; ! ~ */
	
	@Test
	public void testOtherCharacters()	//TODO refactor duplicated code
	{
		try
		{
			for( Integer codePoint = ascii.ALL_CHAR_LOW.val(); 
					     
				 	     codePoint < ascii.ALL_CHAR_HIGH.val() + 1; 
					
						 codePoint++)
			{								
				if( !Character.isLetterOrDigit(codePoint) )
				{
					//convert char[] into Character[]

					char[] letterCharArr = Character.toChars(codePoint);

					Character letter = new Character(letterCharArr[0]);
					PasswordStringBuilder passwordStrBld = 

							new PasswordStringBuilder( letter );

					PasswordEntropy pwTest = new PasswordEntropy( passwordStrBld );

					Integer entropy = pwTest.value(); 

					assertEquals( _errMsg, entropy, (Integer) 4 );
				
				}//if
				
			}//for
			
		}
		catch(Exception anyException)
		{
			fail(_errMsg + " caused abend " + anyException.getMessage());
			
		}//try
		
	}//method
	
}//class
