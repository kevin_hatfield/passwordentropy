package edu.mercer.medapps;

/**
 * Password Entropy analysis 
 * 
 * <ul>
 *
 * <li>created TDD, Java, JUnit
 *
 * <li>TDD Test input, output comes from existing application</li>
 * <pre>#!/usr/bin/perl -l
 * 
 * use DataPasswordEntropy;
 * print password_entropy($ARGV[0]); 
 * exit 0;</pre>
 *
 *</ul>
 * @version pre-beta, development
 * @author Hatfield, Kevin
 */

public class PasswordEntropy implements Config
{	
	/** 
	 * PasswordStringBuilder containing comparable Password based on 
	 * StringBuilder 
	 */
	
	private PasswordStringBuilder _pw;

	/** Integer calculated entropy for password */
	
	private Integer _entropyValue;

	
	/** 
	 * Construct instance of PasswordEntropy 
	 * 
	 * @param pwIn StringBuilder containing password
	 * 
	 * Exception of empty password prevented by isValid() 
	 */
	
	PasswordEntropy(PasswordStringBuilder pwIn)
	{
		if( pwIn != null ) set_pw(pwIn);
		
		if( get_pw().isValid() ){ 
		
			try
			{
			
				set_entropy( computeEntropy() );
		
			}
			catch( Exception unlikelyException )
			{}
				
		}//if
		
	}//constructor

	
	/** encapsulation methods */
	
	
	/** @param pwIn StringBuilder containing password */
	
	private void set_pw(PasswordStringBuilder pwIn){ _pw = pwIn; }//method

	
	/**	@return StringBuilder containing password */
	
	private PasswordStringBuilder get_pw(){ return _pw; }//method
	
	
	/** @param entropyIn Integer calculated entropy value */
	
	private void set_entropy(Integer entropyIn)
	{ 
		_entropyValue = entropyIn; 
	
	}//method
	
	
	/** @return Integer entropy value for StringBuilder password */
	
	private Integer get_entropy(){ return _entropyValue; }

	
	/**
	 * Entropy value of StringBuilderpassword
	 * 
	 * <ul>
	 * <li>error thrown for invalid password input
	 * <li>prevents error from being thrown by constructor
	 * </ul>
	 * 
	 * @throws Exception non-ascii password
	 * @return Integer entropy value for StringBuilder password 
	 */

	protected Integer value() throws Exception
	{ 
		if( !get_pw().isValid() )
		{ 
			throw new Exception("Non-ascii password " + get_pw().toString() 
					
					+ " of length " + get_pw().length() );
			
		}//if
		
		return get_entropy();
		
	}//method	
	

	/** functional methods */
	
		
	/**
	 * compute entropy value instance var password
	 * 
	 * @return Integer password entropy using instance var
	 * @throws Exception 
	 */
	
	private Integer computeEntropy() throws Exception
	{		
		return computeEntropy( get_pw() );
		
	}//method

	
	/**
	 * compute entropy value for non-instance var password
	 * 
	 * @param pwIn StringBuilder password
	 * @return Integer calculated entroy value  
	 * @throws Exception 
	 */
	
	private Integer computeEntropy(PasswordStringBuilder pwIn) throws Exception
	{			
		Integer entropyValue = 0;

		
		if( pwIn.length()==1 )
		{
			if( pwIn.value().codePointAt(0) < ascii.NUMERIC_LOW.val() ||
				
				pwIn.value().codePointAt(0) > ascii.NUMERIC_HIGH.val() )
			{				
				return 4;
				
			}//if
			
			return 3;
		
		}//if

		if( pwIn.length() > 2 )
		{
			throw new Exception("Password length not yet implemented");
		}
		else if( pwIn.length()==2 )
		{
			//two distinct 6 OR two equal 4
			
			if( pwIn.isNumeric() )	//two numeric
			{
				return pwIn.repeats() ? 4 : 6;
				
			}//if
			
			if ( pwIn.repeats() )
			{
				if( pwIn.hasSpaces() ) return 6;	// '  '

				if( pwIn.hasSymbols() ) return 6;	// '##'

				return 7;	//'ss' or 'SS'					

			}//if

			if ( pwIn.hasSpaces() )
			{				
				if( pwIn.hasSymbols() ) return 8;	// '& '

				if( pwIn.hasNumeric() ) return 9;	// ' 7'
	
				return 10;	// 'w '				
				
			}//if

			if ( pwIn.hasNumeric() )
			{				
				if( pwIn.hasSymbols() ) return 9;	// '&3'
	
				return 10;	// 'w4'				
				
			}//if

			if( pwIn.hasSymbols() )
			{
				return 10;	// 'k?'
				
			}//if

			if( pwIn.ucase() || pwIn.lcase() )
			{
				return 9;	// 'IU' or 'kt'
				
			}
			else
			{
				return 11;	// 'iU'
				
			}//if
		}//if

		
		throw new Exception("Fail determine entropy: " + pwIn.toString() );
		
		//return entropyValue;
		
	}//method

		
	/** Java Object overrides and related methods */
	

	/** @return hashcode of StringBuilder password */
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
	
		result = prime * result + 
				((get_pw() == null) ? 0 : get_pw().hashCode());
		
		return result;
	
	}//method


	/** 
	 * Compares objects using '_pw' StringBuilder instance var
	 * 
	 * @param obj object instance
	 * 
	 * @return Boolean equality object type, StringBuilder password equality
	 */
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Boolean equalType = obj instanceof PasswordEntropy;
		
		PasswordEntropy other;
		
		if( equalType )
		{
			other = (PasswordEntropy) obj; 
		}
		else
		{
			return false;
			
		}//if

		if (this.get_pw() == null) 
		{
			if (other.get_pw() != null)
				return false;
		} 
		else if ( this.get_pw() != other.get_pw() )
		{
			return false;

		}//if

		return true;
		
	}//method	
	
	
}//class
