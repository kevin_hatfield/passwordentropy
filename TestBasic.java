package edu.mercer.medapps;

/**
 * 
 * Shared code used by tests: removes specifics of class being tested from
 * individual tests in suite
 * 
 * Each test in suite contains as little code as possible, wth nothing about the
 * PasswordEntropy class being tested
 * 
 * @author Hatfield, Kevin
 */

class TestBasic extends Object
{
	private PasswordEntropy _pwEntropy;
		
	/** 
	 * create TestBasic instance
	 * 
	 * @param pwIn StringBuilder containing password
	 */
	
	TestBasic( PasswordStringBuilder pwIn )
	{		
		set_pwEntropy( new PasswordEntropy( pwIn ) );
	
	}//constructor
	
	
	/** encapsulation methods */
	
	
	/** @param pwEntIn PasswordEntropy instance */
	
	protected void set_pwEntropy( PasswordEntropy pwEntIn )
	{
		_pwEntropy = pwEntIn;
		
	}//method

	/** @return PasswordEntropy instance var */
	
	protected PasswordEntropy get_pwEntropy()
	{
		return _pwEntropy;
		
	}//method
	
	
	/** hash, equals, derived from methods spawned by Eclipse */

	/** 
	 * @return Integer hash code derived from class Stringbuilder password var
	 */
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
	
		result = prime * result
				+ ((get_pwEntropy() == null) ? 0 : get_pwEntropy().hashCode());
		
		return result;
	
	}//method

	/**
	 * @param obj TestBasic object instance
	 * 
	 * @return boolean equality of TestBasic instances, based on 
	 * Stringbuilder password var
	 */

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Boolean sameType = obj instanceof TestBasic;
		
		if(!sameType) return false;
		
		TestBasic other = (TestBasic) obj;
		
		if (get_pwEntropy() == null) 
		{
			if (other.get_pwEntropy() != null)
			{
				return false;
			} 
			else if ( !get_pwEntropy().equals( other.get_pwEntropy() ) )
			{
				return false;
			}//if
			
		}//if
		
		return true;
		
	}//method
				
}//class