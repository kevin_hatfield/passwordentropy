package edu.mercer.medapps;

import edu.mercer.medapps.Config.ascii;

/**
 * <b>PasswordStringBuilder</b>: data class based on StringBuilder
 * 
 * <ul>
 *      <li>variety of Integer, Character single and array constructors
 * 
 *      <li>equals: 
 * 
 *      <ul><li>StringBuilder instance variable:
 *      
 *          <ul><li>length                   
 *              <li>Character code points        
 *          </ul>
 *      </ul>
 *      
 *      <li>hashcode: StringBuilder hashcode used</ul>
 *      
 * @see Config
 * 
 * @author Hatfield, Kevin
 */

public class PasswordStringBuilder implements Config
{	
	/** instance variables */
	
	/** password as StringBuilder */
	
	private StringBuilder _pw = new StringBuilder( new Character('\uFFFD') );
	
	
	/** constructors */
	
	
	/** no arg constructor: create with password as empty StringBuilder */
	
	PasswordStringBuilder()
	{
		set_pw( new StringBuilder( new Character('\uFFFD') ) );
		
	}//constructor

	
	/** 
	 * create passwordStringBuilder instance
	 * 
	 * @param pwIn Character[] containing password 
	 */
	
	PasswordStringBuilder(Character[] pwIn)
	{
		set_pw( pwIn );
		
	}//method

	
	/** 
	 * create passwordStringBuilder instance
	 * 
	 * @param pwIn single Character containing password 
	 */
	
	PasswordStringBuilder(Character pwIn)
	{
		set_pw(pwIn);
		
	}//constructor

	
	/** 
	 * create passwordStringBuilder instance
	 * 
	 * @param pwIn single Integer containing password 
	 */
	
	PasswordStringBuilder(Integer pwIn)
	{
		set_pw(pwIn);
		
	}//constructor
	
	
	/** encapsulation methods */
	
		
	/** 
	 * append character to end of password
	 * 
	 * @param pwIn a single Character
	 */
	
	private void set_pw(Character pwIn)
	{
		_pw.append(pwIn);
		
	}//method	

	
	/** 
	 * append Integer to end of password
	 * 
	 * @param pwIn a single Integer
	 */
	
	private void set_pw(Integer pwIn)
	{
		_pw.append(pwIn);
		
	}//method	
	
	
	/**
	 * overwrite password
	 * 
	 * @param pwIn Character[] containing password 
	 */
	
	private void set_pw(Character[] pwIn)
	{
		for(Integer q=0; q < pwIn.length; q++) 
		{
			set_pw( pwIn[q] );
			
		}//for
		
	}//method


	/** 
	 * overwrite password 
	 * 
	 * @param pwIn StringBuilder containing password 
	 */
	
	protected void set_pw(StringBuilder pwIn)
	{
		_pw = pwIn;
		
	}//method
	
	
	/** @return StringBuilder password */
	
	private StringBuilder get_pw()
	{
		return _pw;
		
	}//method
	
	
	/** @return StringBuilder password */

	protected StringBuilder value()
	{
		return get_pw();
		
	}//method
	
	
	/** predicate methods */
	
	
	/** @return Integer containing StringBuilder password length */
	
	protected Integer length()
	{
		return get_pw().length();
		
	}//method
	
	
	/** @return Boolean password StringBuilder not empty, limited ASCII */
	
	protected boolean isValid()
	{		
		for(int c=0; c < get_pw().length(); c++)
		{
			Integer pwLetterCP = get_pw().codePointAt(c);
			
			if( pwLetterCP < ascii.ALL_CHAR_LOW.val() ||
					
				pwLetterCP > ascii.ALL_CHAR_HIGH.val() )
			{
				return false;
				
			}//if
			
		}//for

		return get_pw().length() > 0 ? true : false;
		
	}//method

	
	/** 
	 * <b>isNumeric</b> analyzes a StringBuilder's characters if numeric, 
	 * limited ascii
	 * 
	 * <ul>
	 * <li>default: PasswordStringBuilder StringBuilder instance var
	 * <li>option: StringBuilder argument
	 * </ul>
	 * 
	 * @return Boolean password composed of only numeric Characters
	 *  
	 * @throws Exception password empty
	 * 
	 * @see Config
	 */
	
	protected boolean isNumeric(StringBuilder... pwIn) throws Exception
	{
		StringBuilder password = pwIn.length > 0 ? pwIn[0] : get_pw(); 
		
		if( password.length() == 0) throw new Exception("Empty password");
		
		for(Integer b=0; b < password.length(); b++)
		{
			Integer codePoint = password.codePointAt(b);
			
			if( codePoint < ascii.NUMERIC_LOW.val() ||
					
					codePoint > ascii.NUMERIC_HIGH.val() )
			{
				return false;
				
			}//if
			
		}//for
				
		return true;
		
	}//method

	
	/** 
	 * <b>hasNumeric</b> analyzes a StringBuilder's characters if any numeric, 
	 * limited ascii
	 * 
	 * <ul>
	 * <li>default: PasswordStringBuilder StringBuilder instance var
	 * <li>option: StringBuilder argument
	 * </ul>
	 * 
	 * @return Boolean password composed of ANY numeric Characters
	 *  
	 * @throws Exception password empty
	 * 
	 * @see Config
	 */
	
	protected boolean hasNumeric(StringBuilder... pwIn) throws Exception
	{
		StringBuilder password = pwIn.length > 0 ? pwIn[0] : get_pw(); 
		
		if( password.length() == 0) throw new Exception("Empty password");
		
		for(Integer b=0; b < password.length(); b++)
		{
			Integer codePoint = password.codePointAt(b);
			
			if( codePoint >= ascii.NUMERIC_LOW.val() &&
					
					codePoint <= ascii.NUMERIC_HIGH.val() )
			{
				return true;
				
			}//if
			
		}//for
				
		return false;
		
	}//method
	
	
	/** 
	 * @return Boolean password composed entrely of repeating Characters 
	 * 
	 * @throws Exception password empty
	 */
	
	protected boolean repeats() throws Exception
	{
		StringBuilder password = get_pw();

		if( password.length() == 0 ) throw new Exception("Empty password");
		
		if( password.length() == 1 ) return false;
		
		Character chrPrevious;
		Character chrCurrent = Character.MAX_VALUE;
		
		for(Integer b=0; b < password.length(); b++)
		{
			chrPrevious = chrCurrent;
			
			chrCurrent = password.charAt(b);
			
			if( b > 0 && chrPrevious != chrCurrent ) return false;
			
		}//for
		
		return true;
		
	}//method

	
	/** 
	 * @return Boolean password is all lowercase
	 * 
	 * @throws Exception password empty
	 */
	
	protected boolean lcase() throws Exception
	{
		StringBuilder password = get_pw();

		if( password.length() == 0 ) throw new Exception("Empty password");

		Boolean lowCase = true;
		
		for( Integer b=0; b < password.length(); b++ )
		{
			if( Character.isUpperCase( password.charAt(b) ) )
			{
				lowCase = false;
				break;
				
			}//if
	
		}//for
		
		return lowCase;
		
	}//method

	
	/** 
	 * @return Boolean password is all uppercase
	 * 
	 * @throws Exception password empty
	 */
	
	protected boolean ucase() throws Exception
	{
		StringBuilder password = get_pw();

		if( password.length() == 0 ) throw new Exception("Empty password");

		Boolean upCase = true;
		
		for( Integer b=0; b < password.length(); b++ )
		{
			if( Character.isLowerCase( password.charAt(b) ) )
			{
				upCase = false;
				break;
				
			}//if
	
		}//for
		
		return upCase;
		
	}//method

	
	/** 
	 * @return Boolean password is all space Characters
	 * 
	 * @throws Exception password empty
	 */
	
	protected boolean allSpaces() throws Exception
	{
		StringBuilder password = get_pw();

		if( password.length() == 0 ) throw new Exception("Empty password");

		Boolean onlySpaces = true;
		
		for( Integer b=0; b < password.length(); b++ )
		{
			if( ! Character.isSpaceChar( password.charAt(b) ) )
			{
				onlySpaces = false;
				break;
				
			}//if
	
		}//for
		
		return onlySpaces;
		
	}//method


	/** 
	 * @return Boolean password has any blank Characters
	 * 
	 * @throws Exception password empty
	 */
	
	protected boolean hasSpaces() throws Exception
	{
		StringBuilder password = get_pw();

		if( password.length() == 0 ) throw new Exception("Empty password");

		Boolean hasSpaces = false;
		
		for( Integer b=0; b < password.length(); b++ )
		{
			if( Character.isSpaceChar( password.charAt(b) ) )
			{
				hasSpaces = true;
				break;
				
			}//if
	
		}//for
		
		return hasSpaces;
		
	}//method


	/** 
	 * @return Boolean password has any symbol Characters
	 * 
	 * @throws Exception password empty
	 */
	
	protected boolean hasSymbols() throws Exception
	{
		StringBuilder password = get_pw();

		if( password.length() == 0 ) throw new Exception("Empty password");

		Boolean hasSymbols = false;
		
		for( Integer b=0; b < password.length(); b++ )
		{
			if( ! Character.isSpaceChar( password.charAt(b) ) &&
					
					! Character.isLetterOrDigit( password.charAt(b) ) )
			{
				hasSymbols = true;
				break;
				
			}//if
	
		}//for
		
		return hasSymbols;
		
	}//method


	/** 
	 * @return Boolean password has only symbol Characters
	 * 
	 * @throws Exception password empty
	 */
	
	protected boolean allSymbols() throws Exception
	{
		StringBuilder password = get_pw();

		if( password.length() == 0 ) throw new Exception("Empty password");

		Boolean allSymbols = true;
		
		for( Integer b=0; b < password.length(); b++ )
		{
			if( Character.isSpaceChar( password.charAt(b) ) ||
					
					Character.isLetterOrDigit( password.charAt(b) ) )
			{
				allSymbols = false;
				break;
				
			}//if
	
		}//for
		
		return allSymbols;
		
	}//method

	
	/** Java Object overrides and related methods */

	
	/** 
	 * allow StringBuilder comparison
	 * 
	 * @param strBldr1 StringBuilder object instance
	 * @param strBldr2 StringBuilder object instance
	 * @return Boolean equality of length and character code comparisons
	 */
	
	private boolean compareStrBuilders(StringBuilder strBldr1, 
			StringBuilder strBldr2)
	{
		boolean result = false;
		
		if( strBldr1.length() == strBldr2.length() ) 
		{
			for(int x=0; x<strBldr1.length(); x++)
			{
				if( strBldr1.codePointAt(x) != strBldr2.codePointAt(x) )
				{
					return false;
					
				}//if
				
			}//for

			result = true;
			
		}//if
	
		return result;
		
	}//method
	

	/** @return hashcode of StringBuilder password */
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
	
		result = prime * result + 
				((get_pw() == null) ? 0 : get_pw().hashCode());
		
		return result;
	
	}//method


	/** 
	 * Compares objects using '_pw' StringBuilder instance var
	 * 
	 * @param obj object instance
	 * 
	 * @return Boolean equality object type, StringBuilder password equality
	 */
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Boolean equalType = obj instanceof PasswordStringBuilder;
		
		PasswordStringBuilder other;
		
		if( equalType )
		{
			other = (PasswordStringBuilder) obj; 
		}
		else
		{
			return false;
			
		}//if

		if (this.value() == null) 
		{
			if (other.value() != null)
				return false;
		} 
		else if ( !compareStrBuilders(this.value(),other.value()) )
		{
			return false;

		}//if

		return true;
		
	}//method	

	
}//class
