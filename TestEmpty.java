package edu.mercer.medapps;

import static org.junit.Assert.*;

import org.hamcrest.SelfDescribing;
import org.junit.Before;
import org.junit.Test;

/**
 * Test: empty password
 * 
 * @author Hatfield, Kevin
 */

public class TestEmpty
{	
	private static final String _errMsg = "Test Empty";
	
	private static final PasswordStringBuilder _equalData  
	
		= new PasswordStringBuilder();

	private static final PasswordStringBuilder _unEqualData  
	
		= new PasswordStringBuilder(new Character('\uFFFD'));
		
	private static final PasswordEntropy _testFramework 
	
		= new PasswordEntropy( _equalData );
	
	
	/** encapsulation methods */

	// unneeded: instance variables final
	
	
	/** functional methods */

	
	/** verify successful object creation for test class */
	
	@Before
	public void setUp() 
	{	
		//valid outside try block: constructors should not throw exceptions

		assertEquals( _errMsg, _testFramework , 
				
				new PasswordEntropy(_equalData) );
		
		assertNotEquals( _errMsg, _testFramework, 
				
				new PasswordEntropy(_unEqualData) );
		
	}//method

	
	/** test against known- expected, desired -input and output */
	
	@Test
	public void test()
	{
		try
		{
			_testFramework.value();
			
			fail(_errMsg);
		}
		catch (Exception anyException)
		{
			
			
		}//try
		
	}//method
	
}//class
